import React from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Text, TouchableOpacity, View, Image } from 'react-native';
import Home from '../screens/ScreenHome';
import Transaction from '../screens/ScreenTransaction';
import Profil from '../screens/ScreenProfile';
import Icon from 'react-native-vector-icons/Entypo';

const Tab = createBottomTabNavigator();


function MyTabBar({ state, descriptors, navigation }) {
  return (
    <View style={{ flexDirection: 'row', borderTopLeftRadius: 14, borderTopRightRadius: 14, height: 82 }}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({ name: route.name, merge: true });
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        const iconTab =
          label === 'Home' ? (
            <Icon name="home" size={24} />
          ) : label === 'Transaction' ? (
            <Icon name="document" size={24} />
          ) : label === 'Profile' ? (
            <Icon name="user" size={24} />
          ) : null;

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}
          >
            <Image source={iconTab} style={{ height: 20, width: 20 }} />
            <Text style={{ color: isFocused ? 'red' : 'black', marginTop: 2 }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

function BottomTab() {
  return (
    <Tab.Navigator tabBar={props => <MyTabBar {...props} />} screenOptions={{ headerShown: false }}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Transaction" component={Transaction} />
      <Tab.Screen name="Profil" component={Profil} />
    </Tab.Navigator>
  );
}

export default BottomTab