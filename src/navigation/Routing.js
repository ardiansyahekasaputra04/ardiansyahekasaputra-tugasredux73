import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import AuthNavigation from "./AuthNavigation";
import ScreenLoading from "../screens/ScreenLoading";
import BottomTab from "./BottomTabs";
import Detail1 from "../screens/ScreenDetail1";
import Detail2 from "../screens/ScreenDetail2";
import Pemesanan from "../screens/ScreenFormPemesanan";
import Keranjang from "../screens/ScreenKeranjang";
import Summary from "../screens/ScreenSummary";
import Berhasil from "../screens/ScreenBerhasil";
import Reservasi from "../screens/ScreenReservasi";
import Edit from "../screens/ScreenEdit";
// import DataBarang from "../adder/DataBarang";
import DataToko from "../adder/AddToko";
import DataKeranjang from "../adder/DataKeranjang";

const Stack = createNativeStackNavigator();

export default function Routing() {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name="ScreenLoading" component={ScreenLoading} />
                <Stack.Screen name="AuthNavigation" component={AuthNavigation} />
                <Stack.Screen name="Detail1" component={Detail1} />
                <Stack.Screen name="Detail2" component={Detail2} />
                <Stack.Screen name="Pemesanan" component={Pemesanan} />
                <Stack.Screen name="Keranjang" component={Keranjang} />
                <Stack.Screen name="Summary" component={Summary} />
                <Stack.Screen name="Berhasil" component={Berhasil} />
                <Stack.Screen name="Reservasi" component={Reservasi} />
                <Stack.Screen name="Edit" component={Edit} />
                <Stack.Screen name="BottomTab" component={BottomTab} />
                <Stack.Screen name="AddToko" component={DataToko} />
                <Stack.Screen name="DataKeranjang" component={DataKeranjang} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}