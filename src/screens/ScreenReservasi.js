import moment from "moment/moment";
import React from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground,
    FlatList
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import Icons from "react-native-vector-icons/FontAwesome";
import { useSelector } from "react-redux";

const Reservasi = ({
    navigation,
    route
}) => {
    const { barang } = useSelector((state) => state.barang)
    return (
        <View style={styles.container}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}>
                <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    keyboardVerticalOffset={-500}>
                    <View style={styles.navigasi}>
                        <TouchableOpacity onPress={() => navigation.navigate('BottomTab')}>
                            <Icon name="arrowleft" size={20} color={'black'} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.kode}>
                        <Text style={styles.tanggal}>{moment().format('DD MMMM YYYY HH:mm')}</Text>
                        <Text style={styles.nomorKode}>CS{moment().format('MMYYYY')}</Text>
                        <Text style={styles.nama}>Kode Reservasi</Text>
                        <Text style={styles.syarat}>Sebutkan Kode Reservasi saat tiba di outlet</Text>
                    </View>
                    <Text style={styles.barang}>Barang</Text>
                    <View style={{ marginHorizontal: 11 }}>
                        <FlatList
                            data={barang}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => (
                                <View
                                    style={styles.rekomenTempat}
                                >
                                    <Image
                                        source={{ uri: item.fotoBarang }}
                                        style={styles.produk}
                                    />
                                    <View
                                        style={styles.deskripsi}
                                    >
                                        <Text style={styles.teks}>{item.merek}-{item.warna}-{item.ukuran}</Text>
                                        <Text style={styles.teks1}>{item.pilihan}</Text>
                                        <Text style={styles.teks1}>Note: {item.catatan}</Text>
                                    </View>
                                </View>
                            )}
                        />
                    </View>
                    <Text style={styles.status}>Status Pesanan</Text>
                    <View style={{ marginHorizontal: 11 }}>
                        <View style={styles.statusView}>
                            <View style={styles.tanda}>
                                <Icons name="circle" size={10} color='red' />
                            </View>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={styles.statusReservasi}>Telah Reservasi</Text>
                                <Text style={styles.tanggalReservasi}>{moment().format('DD MMMM YYYY')}</Text>
                            </View>
                            <Text style={styles.waktuReservasi}>{moment().format('HH:mm')}</Text>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    )
}

export default Reservasi

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f8ff',
    },
    arrow: {
        width: 16,
        height: 12,
        resizeMode: 'contain',
    },
    navigasi: {
        width: Dimensions.get("screen").width,
        height: 56,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 25,
        flexDirection: 'row',
        borderBottomWidth: 1,
        shadowColor: '0px 4px 4px rgba(220, 220, 220, 0.25);'
    },
    kode: {
        width: Dimensions.get('screen').width,
        height: 238,
        backgroundColor: '#fff'
    },
    tanggal: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 13,
        lineHeight: 15,
        textAlign: 'center',
        color: '#BDBDBD',
        marginTop: 16
    },
    nomorKode: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 36,
        letterSpacing: 0.257143,
        color: '#201F26',
        textAlign: 'center',
        marginTop: 57
    },
    nama: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        letterSpacing: 0.257143,
        color: '#000000',
        textAlign: 'center',
        marginTop: 13
    },
    syarat: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        letterSpacing: 0.257143,
        color: '#6f6f6f',
        textAlign: 'center',
        marginTop: 42
    },
    barang: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        letterSpacing: 0.257143,
        color: '#201F26',
        marginTop: 18,
        marginLeft: 11
    },
    rekomenTempat: {
        width: '100%',
        height: 133,
        marginTop: 16,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 15,
    },
    produk: {
        height: 84,
        width: 84,
        marginLeft: 14,
        borderRadius: 5
    },
    deskripsi: {
        width: 200,
        height: 80,
        marginLeft: 15,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    teks: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 15,
        letterSpacing: 1,
        color: '#000000',
    },
    teks1: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '400',
        lineHeight: 15,
        letterSpacing: 1,
        color: '#737373',
        marginTop: 11
    },
    status: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        letterSpacing: 0.257143,
        color: '#201F26',
        marginLeft: 11
    },
    statusView: {
        width: '100%',
        height: 78,
        marginTop: 16,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 43,
    },
    tanda: {
        width: 15,
        height: 15,
        marginLeft: 18,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(187, 36, 39, 0.5)',
        borderRadius: 20
    },
    statusReservasi: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        letterSpacing: 0.257143,
        color: '#201F26',
        marginLeft: 11
    },
    tanggalReservasi: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 10,
        letterSpacing: 0.257143,
        color: '#a5a5a5',
        marginLeft: 11
    },
    waktuReservasi: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 10,
        letterSpacing: 0.257143,
        color: '#a5a5a5',
        marginLeft: 172
    },
})