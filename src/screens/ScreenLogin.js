import React, { useState } from "react";
import {
   View,
   Text,
   ScrollView,
   KeyboardAvoidingView,
   Image,
   TextInput,
   TouchableOpacity,
   StyleSheet,
   Dimensions,
   Alert
} from "react-native";
import Icon from 'react-native-vector-icons/AntDesign'
import { useDispatch, useSelector } from "react-redux";

const Login = ({
   navigation,
   route
}) => {
   // const [email, setEmail] = useState('')
   //  const [password, setPassword] = useState('')
    const dispatch = useDispatch()
    const { isLoggedIn, /* account */ } = useSelector((state) => state.account);
    
    React.useEffect(() => { if (isLoggedIn) { navigation.replace('BottomTab') } }, [])
    
    // const url = 'https://staging.api.autotrust.id/api/v1/';
    const onLogin = async () => {
      //   const data = { email, password }
        try {
            // const response = await fetch(`${url}user/login`, {
            //     method: "POST",
            //     headers: {
            //         "Content-Type": "application/json",
            //     },
            //     body: JSON.stringify(data),
            // });
            // const result = await response.json();
            console.log("Success:", result);
            if (result.code === 200) {
                const data = result
                dispatch({ type: 'LOGIN_SUCCESS', data })
                ToastAndroid.showWithGravity('Login Sukses', ToastAndroid.LONG, ToastAndroid.BOTTOM,);
                navigation.replace('BottomTab', data)
            }
        } catch (error) {
            console.error("Error:", error);
            ToastAndroid.showWithGravity(
                'Login gagal, email atau password salah',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,);
        }
    }

   return (
      <View style={styles.container}>
         <ScrollView //component yang digunakan agar tampilan kita bisa discroll
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: 10 }}
         >
            <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
               behavior='padding' //tampilan form atau text input
               enabled
               keyboardVerticalOffset={-500}
            >
               <Image
                  source={require('../assets/image/LoginBackground.png')} //load atau panggil asset image dari local
                  style={styles.background}
               />
               <View style={styles.input}>
                  <Text style={styles.title1}>Welcome Guest,</Text>
                  <Text style={styles.title2}>Please Login First</Text>
                  <Text style={styles.email}>
                     Email
                  </Text>
                  <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                     placeholder='Masukkan Email' //pada tampilan ini, kita ingin user memasukkan email
                     style={styles.emailInput}
                     caretHidden={false}
                     // value={account}
                     keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
                  />
                  <Text style={styles.password}>
                     Password
                  </Text>
                  <TextInput //component yang digunakan untuk memasukkan data password
                     placeholder='Masukkan Password'
                     secureTextEntry //props yang digunakan untuk menyembunyikan password user
                     style={styles.passwordInput}
                     // value={account}
                  />
                  <View style={styles.backgroundButton}>
                     <View style={styles.buttonLogo}>
                        {
                           //component TouchableOpacity, kita gunakan sebagai tombol
                           //menggunakan component ini sebagai tombol, karena mudah untuk di atur style dan kegunaanya
                        }
                        <TouchableOpacity onPress={() => navigation.navigate('')}>
                           <Icon
                              name="google" //load asset dari local
                              size={30}
                              color={'red'}
                           />
                        </TouchableOpacity>
                        <TouchableOpacity>
                           <Icon name="facebook-square" size={30} color={'blue'} style={{ marginHorizontal: 15 }}
                           />
                        </TouchableOpacity>
                        <TouchableOpacity>
                           <Icon name='twitter' size={30} color={'aqua'}
                           />
                        </TouchableOpacity>
                     </View>
                     <TouchableOpacity
                        style={styles.buttonForgot}
                        onPress={() => navigation.navigate('Reset')}
                     >
                        <Text style={styles.forgotText}>
                           Forgot Password?
                        </Text>
                     </TouchableOpacity>
                  </View>
                  <TouchableOpacity
                     style={styles.loginButton}
                     onPress={() => navigation.navigate('BottomTab')}
                  >
                     <Text style={styles.loginText}>
                        Login
                     </Text>
                  </TouchableOpacity>
                  <View style={styles.registerNavigate}>
                     <Text style={styles.registerQuestion}>
                        Don't Have An Account yet?
                     </Text>
                     <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                        <Text style={styles.registerText}>
                           Register
                        </Text>
                     </TouchableOpacity>
                  </View>
               </View>
            </KeyboardAvoidingView>
         </ScrollView>
      </View>
   )
}

export default Login

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
   },
   background: {
      width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
      height: 317,
   },
   title1: {
      fontSize: 30,
      // fontWeight: 'bold',
      color: '#0A0827',
      fontFamily: 'Montserrat-Bold'
   },
   title2: {
      fontSize: 30,
      fontWeight: 'bold',
      color: '#0A0827'
   },
   input: {
      width: '100%',
      backgroundColor: '#fff',
      borderTopLeftRadius: 19,
      borderTopRightRadius: 19,
      paddingHorizontal: 20,
      paddingTop: 38,
      marginTop: -20
   },
   email: {
      color: 'red',
      fontWeight: 'bold',
      marginTop: 25
   },
   emailInput: {
      marginTop: 11,
      width: '100%',
      borderRadius: 8,
      backgroundColor: '#F6F8FF',
      paddingHorizontal: 10
   },
   password: {
      color: 'red',
      fontWeight: 'bold',
      marginTop: 11,
   },
   passwordInput: {
      marginTop: 11,
      width: '100%',
      borderRadius: 8,
      backgroundColor: '#F6F8FF',
      paddingHorizontal: 10
   },
   backgroundButton: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 15,
      justifyContent: 'space-between',
   },
   buttonLogo: {
      flexDirection: 'row',
      alignItems: 'center'
   },
   facebook: {
      width: 36,
      height: 25,
      marginHorizontal: 15,
      resizeMode: 'contain'
   },
   twitter: {
      width: 36,
      height: 25,
      resizeMode: 'contain'
   },
   buttonForgot: {
      flexDirection: 'row',
      alignItems: 'center',
   },
   forgotText: {
      fontSize: 12,
      color: '#717171'
   },
   loginButton: {
      width: '100%',
      marginTop: 77,
      backgroundColor: '#BB2427',
      borderRadius: 8,
      paddingVertical: 15,
      justifyContent: 'center',
      alignItems: 'center'
   },
   loginText: {
      color: '#fff',
      fontSize: 16,
      fontWeight: 'bold'
   },
   registerNavigate: {
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 30,
      marginBottom: 23,
      flexDirection: 'row',
   },
   registerQuestion: {
      fontSize: 12,
      color: '#717171'
   },
   registerText: {
      fontSize: 14,
      color: '#BB2427',
      marginLeft: 5
   },
});