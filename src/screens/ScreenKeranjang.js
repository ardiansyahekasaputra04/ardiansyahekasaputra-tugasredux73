import React from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/AntDesign";
import { useSelector } from "react-redux";

const Keranjang = ({
    navigation,
    route
}) => {
    const { barang } = useSelector((state) => state.barang)

    return (
        <View style={styles.container}>
            <View style={styles.navigasi}>
                <TouchableOpacity onPress={() => navigation.navigate('Detail1')}>
                    <Icon
                        name="arrowleft" size={20} color={'black'}
                    />
                </TouchableOpacity>
                <Text style={styles.teksJudul}> Keranjang </Text>
            </View>
            <View style={{ paddingHorizontal: 20, height: '79%', marginTop: 20}}>
                <View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <FlatList
                            data={barang}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity
                                    style={styles.rekomenTempat}
                                    onPress={() => navigation.navigate('Pemesanan', { item })}
                                >
                                    <Image
                                        source={{ uri: item.fotoBarang }}
                                        style={styles.produk}
                                    />
                                    <View
                                        style={styles.deskripsi}
                                    >
                                        <Text style={styles.teks}>{item.merek}-{item.warna}-{item.ukuran}</Text>
                                        <Text style={styles.teks1}>{item.pilihan}</Text>
                                        <Text style={styles.teks1}>Note: {item.catatan}</Text>
                                        <Text style={styles.teks1}>ID: {item.id}</Text>
                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                        <TouchableOpacity style={styles.plus} onPress={() => navigation.navigate('DataKeranjang')}>
                            <Icon name="plussquareo" size={30} color={'red'} />
                            <Text style={styles.plusText}>Tambah Keranjang</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </View>
            <View style={{ paddingHorizontal: 20, alignContent: 'center' }}>
                <TouchableOpacity
                    style={styles.nextButton}
                    onPress={() => navigation.navigate('Summary')}>
                    <Text style={styles.nextText}>
                        Selanjutnya
                    </Text>
                </TouchableOpacity>
            </View>
        </View >
    )
}

export default Keranjang

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f8ff',
    },
    arrow: {
        width: 16,
        height: 12,
        resizeMode: 'contain',
    },
    navigasi: {
        width: Dimensions.get("screen").width,
        height: 56,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 25,
        flexDirection: 'row',
        borderBottomWidth: 1,
        shadowColor: '0px 4px 4px rgba(220, 220, 220, 0.25);'
    },
    teksJudul: {
        fontFamily: 'Montserrat',
        fontSize: 18,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 20,
        letterSpacing: 1,
        color: '#201f26',
        marginLeft: 14
    },
    rekomenTempat: {
        width: '100%',
        height: 133,
        backgroundColor: '#fff',
        borderRadius: 9,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: 15,
    },
    produk: {
        height: 84,
        width: 84,
        marginLeft: 14,
        borderRadius: 5
    },
    deskripsi: {
        width: 200,
        height: 80,
        marginLeft: 15,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    teks: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 15,
        letterSpacing: 1,
        color: '#000000',
    },
    teks1: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '400',
        lineHeight: 15,
        letterSpacing: 1,
        color: '#737373',
        marginTop: 11
    },
    plus: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#f6f8ff'
    },
    plusLogo: {
        width: 20,
        height: 20
    },
    plusText: {
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 20,
        letterSpacing: 1,
        color: '#737373',
        marginLeft: 8,
        color: 'red'
    },
    nextButton: {
        width: '100%',
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        marginLeft: 20,
        marginTop: 15
    },
    nextText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
})