import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    ScrollView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    FlatList
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import Icons from "react-native-vector-icons/AntDesign"
import { useSelector } from "react-redux";
import { Rating } from "../ratings";

const Home = ({ navigation, route }) => {
    const { toko } = useSelector((state) => state.toko);
    const { account } = useSelector((state) => state.account);
    const [isFavorite, setIsFavorite] = React.useState(false);

    const toggleFavorite = () => {
        setIsFavorite(!isFavorite);
    };
    return (
        <View
            style={styles.container}
        >
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}
            >
                <View
                    style={styles.backgroundAtas}
                >
                    <View
                        style={styles.profilDanBag}
                    >
                        <TouchableOpacity>
                            <Image
                                source={require('../assets/image/Profil.png')}
                                style={styles.profil}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Keranjang')}
                        >
                            <Icon
                                name="shopping-bag"
                                size={30}
                                color={'black'}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text
                        style={styles.ucapan}
                    >
                        Hello, {account.nama}!
                    </Text>
                    <Text
                        style={styles.tawaran}

                    >

                        Ingin merawat dan perbaiki sepatumu? Cari disini
                    </Text>
                    <View
                        style={styles.kolomPencarian}
                    >
                        <View
                            style={styles.kP1}
                        >
                            <Icon
                                name="search"
                                size={20}
                                color={'black'}
                            />
                            <TextInput
                                style={styles.inputPencarian}
                            />
                        </View>
                        <View
                            style={styles.kP2}
                        >
                            <TouchableOpacity>
                                <Icon
                                    name="filter"
                                    size={20}
                                    color={'black'}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View
                    style={styles.backgroundTengah}
                >
                    <ScrollView
                        style={styles.scrollHorizontal}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                    >
                        <TouchableOpacity>
                            <View
                                style={styles.icon}
                            >
                                <Image
                                    source={require('../assets/icon/Sepatu.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    Sepatu
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.iconDenganMargin}
                            >
                                <Image
                                    source={require('../assets/icon/Jaket.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    Jaket
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.icon}
                            >
                                <Image
                                    source={require('../assets/icon/Tas.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    Tas
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.iconDenganMargin}
                            >
                                <Image
                                    source={require('../assets/icon/Jeans.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    Jeans
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.icon}
                            >
                                <Image
                                    source={require('../assets/icon/HP.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    HP
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.iconDenganMargin}
                            >
                                <Image
                                    source={require('../assets/icon/Laptop.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    Laptop
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.icon}
                            >
                                <Image
                                    source={require('../assets/icon/Kulkas.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    Kulkas
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.iconDenganMargin}
                            >
                                <Image
                                    source={require('../assets/icon/JamTangan.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    JamTangan
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.icon}
                            >
                                <Image
                                    source={require('../assets/icon/Vape.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    Vape
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.iconDenganMargin}
                            >
                                <Image
                                    source={require('../assets/icon/Helm.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    CuciHelm
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.icon}
                            >
                                <Image
                                    source={require('../assets/icon/Mobil.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    CuciMobil
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.iconDenganMargin}
                            >
                                <Image
                                    source={require('../assets/icon/AC.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    CuciAC
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.icon}
                            >
                                <Image
                                    source={require('../assets/icon/Motor.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    CuciMotor
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.iconDenganMargin}
                            >
                                <Image
                                    source={require('../assets/icon/Followers.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    CariFollower
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View
                                style={styles.icon}
                            >
                                <Image
                                    source={require('../assets/icon/Lainnya.png')}
                                    style={styles.gambarIcon}
                                />
                                <Text
                                    style={styles.namaIcon}
                                >
                                    Lainnya
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
                <View
                    style={styles.rekomen}
                >
                    <Text
                        style={styles.teksRekomen}
                    >
                        Rekomendasi Terdekat
                    </Text>
                    <TouchableOpacity>
                        <Text
                            style={styles.teksViewAll}
                        >
                            View All
                        </Text>
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={toko}
                    keyExtractor={(
                        item,
                        index
                    ) => index.toString()}
                    renderItem={({
                        item,
                        index
                    }) => (
                        <TouchableOpacity
                            style={styles.rekomenTempat}
                            onPress={() => navigation.navigate(
                                'Detail1',
                                item
                            )}
                        >
                            <Image
                                source={{ uri: item.storeImage }}
                                style={styles.tempat}
                            />
                            <View
                                style={styles.deskripsi}
                            >
                                <View
                                    style={styles.review}
                                >
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={styles.stars}>
                                            <Rating rating={item.rating}
                                                colorOn='yellow'
                                                iconsize={12} />
                                        </View>
                                    </View>
                                    <TouchableOpacity onPress={toggleFavorite}>
                                        <Icons
                                            name={isFavorite ? 'heart' : 'hearto'}
                                            size={12}
                                            color={isFavorite ? 'red' : 'grey'}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <Text
                                    style={styles.teksRating}
                                >
                                    {item.rating}
                                </Text>
                                <Text
                                    style={styles.nama}
                                >
                                    {item.storeName}
                                </Text>
                                <Text
                                    style={styles.alamat}
                                >
                                    {item.address}
                                </Text>
                                <View>
                                    <Text
                                        style={item.isOpen ? (item.isOpen == 'Buka' ? (styles.teksBuka) : (styles.teksTutup)) : ({ backgroundColor: '#fff' })}
                                    >
                                        {item.isOpen}
                                    </Text>
                                </View>
                                <Text style={styles.id}>ID: {item.id}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </ScrollView>
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.btnFloating}
                onPress={() => navigation.navigate('AddToko')}
            >
                <Icon
                    name='plus'
                    size={25}
                    color="#fff"
                />
            </TouchableOpacity>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f8ff',
    },
    backgroundAtas: {
        width: Dimensions.get('screen').width,
        height: 275,
        backgroundColor: '#ffffff'
    },
    profilDanBag: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 56,
        justifyContent: 'space-between',
        paddingLeft: 22,
        paddingRight: 34,
    },
    profil: {
        width: 45,
        height: 45,
        resizeMode: 'contain',
    },
    bag: {
        width: 18,
        height: 20,
        resizeMode: 'contain',
    },
    ucapan: {
        left: 22,
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 15,
        lineHeight: 36,
        color: '#034262'
    },
    tawaran: {
        left: 22,
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 20,
        lineHeight: 33,
        color: '#0a0827'
    },
    kolomPencarian: {
        paddingLeft: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 41,
        width: '100%'
    },
    kP1: {
        width: 255,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        justifyContent: 'space-between',
        paddingLeft: 12,
        paddingRight: 15,
        borderRadius: 14,
        backgroundColor: '#f6f8ff',
        borderWidth: 0,
        height: 45,
    },
    search: {
        width: 17,
        height: 17,
    },
    inputPencarian: {
        marginTop: 1,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 10
    },
    kP2: {
        width: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 15,
        borderRadius: 14,
        backgroundColor: '#f6f8ff',
        borderWidth: 0,
        height: 45,
    },
    filter: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
    },
    backgroundTengah: {
        flexDirection: 'row',
        marginHorizontal: 15,
        marginTop: 42,
    },
    scrollHorizontal: {
        flex: 1
    },
    icon: {
        width: 95,
        height: 95,
        borderRadius: 16,
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    iconDenganMargin: {
        width: 95,
        height: 95,
        borderRadius: 16,
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginHorizontal: 25
    },
    gambarIcon: {
        width: 45,
        height: 45,
        marginHorizontal: 25,
        resizeMode: 'contain'
    },
    namaIcon: {
        fontFamily: 'Montserrat',
        fontSize: 11,
        fontStyle: 'normal',
        fontWeight: '600',
        lineHeight: 11,
        display: 'flex',
        textAlign: 'center',
        color: '#BB2427'
    },
    rekomen: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 27,
        justifyContent: 'space-between',
        paddingHorizontal: 20,
    },
    teksRekomen: {
        fontFamily: 'Montserrat',
        fontSize: 15,
        fontStyle: 'normal',
        fontWeight: '600',
        lineHeight: 15,
        textAlign: 'center',
        color: '#0A0827'
    },
    teksViewAll: {
        fontFamily: 'Montserrat',
        fontSize: 15,
        fontStyle: 'normal',
        fontWeight: '600',
        lineHeight: 15,
        textAlign: 'center',
        color: '#E64C3C',
    },
    rekomenTempat: {
        width: 335,
        height: 133,
        marginHorizontal: 13,
        marginTop: 25,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    tempat: {
        height: 121,
        width: 80,
        marginLeft: 6,
        borderRadius: 5
    },
    deskripsi: {
        width: 234,
        height: 133,
        marginHorizontal: 15,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    review: {
        width: 234,
        height: 12,
        marginTop: 14,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingRight: 10
    },
    rating: {
        width: 50,
        height: 7
    },
    hatiMerah: {
        width: 13,
        height: 12,
    },
    hatiPutih: {
        width: 13,
        height: 12,
        right: 9
    },
    teksRating: {
        height: 20,
        width: 70,
        fontFamily: 'Montserrat',
        fontSize: 10,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 20,
        color: '#d8d8d8'
    },
    nama: {
        width: 255,
        height: 34,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '600',
        lineHeight: 17,
        letterSpacing: 1,
        color: '#201f26'
    },
    alamat: {
        width: 226,
        height: 20,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 20,
        color: '#d8d8d8',
        marginTop: -20
    },
    teksTutup: {
        width: 47,
        height: 15,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 17,
        letterSpacing: 1,
        color: '#ea3d3d',
        backgroundColor: 'rgba(230, 76, 60, 0.2)',
        borderRadius: 10,
        textAlignVertical: 'center',
        textAlign: 'center',
    },
    teksBuka: {
        width: 47,
        height: 15,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 17,
        letterSpacing: 1,
        color: '#11a84e',
        backgroundColor: 'rgba(17, 168, 78, 0.12)',
        borderRadius: 10,
        textAlignVertical: 'center',
        textAlign: 'center',
    },
    btnFloating: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10
    },
    cardItem: {
        width: '90%',
        alignSelf: 'center',
        marginTop: 15,
        borderRadius: 4,
        borderColor: '#dedede',
        borderWidth: 1,
        padding: 15
    },
    id: {
        height: 20,
        width: 70,
        fontFamily: 'Montserrat',
        fontSize: 10,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 20,
        color: '#d8d8d8',
        marginTop: 5
    }
})