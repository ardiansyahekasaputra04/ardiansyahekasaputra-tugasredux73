import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from "react-native";
import Icon from 'react-native-vector-icons/Entypo'

const Profil = ({
    navigation,
    route
}) => {
    const [nama, setNama] = useState('')
    const [email, setEmail] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('');
    const [address, setAddress] = useState('');
    
    const getData = () => {
        try {
            AsyncStorage.getItem('Username')
                .then(value => {
                    if (value != null) {
                        setNama(value)
                    }
                })
            AsyncStorage.getItem('Email')
                .then(value => {
                    if (value != null) {
                        setEmail(value)
                    }
                })
            AsyncStorage.getItem('phoneNumber')
                .then(value => {
                    if (value != null) {
                        setPhoneNumber(value)
                    }
                })
            AsyncStorage.getItem('alamat')
                .then(value => {
                    if (value != null) {
                        setAddress(value)
                    }
                })
        } catch (error) {
            console.log(error)
        }
    }
    useEffect(() => {
        getData()
    }, [])
    const deleteData = () => {
        try {
            AsyncStorage.removeItem('Username')
                .then(() => {
                    setNama('')
                })
            AsyncStorage.removeItem('Email')
                .then(() => {
                    setEmail('')
                })
            AsyncStorage.removeItem('phoneNumber')
                .then(() => {
                    setPhoneNumber('')
                })
            AsyncStorage.removeItem('alamat')
                .then(() => {
                    setAddress('')
                })
            navigation.navigate('Login')
        } catch (error) {
            console.log(error)
        }
    }
    
    return (
        <View style={styles.container}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}>
                <View style={styles.profil}>
                    <Image source={require('../assets/image/Profil.png')} style={styles.foto} />
                    <Text style={styles.nama}>{nama}</Text>
                    <Text style={styles.email}>{email}</Text>
                    <Text style={styles.email}>{address}</Text>
                    <Text style={styles.email}>{phoneNumber}</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('Edit')}>
                        <Text style={styles.Bukabck}> Edit </Text>
                    </TouchableOpacity>
                </View>
                <View style={{ paddingHorizontal: 19 }}>
                    <View style={styles.option}>
                        <TouchableOpacity>
                            <Text style={styles.about}>About</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.opsi}>Terms & Condition</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.opsi}>FAQ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.opsi}>History</Text>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.opsi}>Setting</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 19 }}>
                    <View style={styles.logOut}>
                        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={deleteData}>
                            <Icon name="log-out" size={15} color={'red'} style={{ marginRight: 5 }} />
                            <Text style={styles.out}>Logout</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default Profil

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f8ff',
    },
    profil: {
        width: Dimensions.get('screen').width,
        height: 292,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: '#fff'
    },
    foto: {
        width: 95,
        height: 95
    },
    nama: {
        fontFamily: 'Montserrat',
        fontSize: 20,
        fontWeight: '700',
        lineHeight: 36,
        letterSpacing: 0,
        textAlign: 'left',
        marginTop: 5
    },
    email: {
        fontFamily: 'Montserrat',
        fontSize: 10,
        fontWeight: '500',
        lineHeight: 12,
        letterSpacing: 0,
        textAlign: 'left',
    },
    Bukabck: {
        width: 64,
        height: 27,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 17,
        letterSpacing: 1,
        color: '#050152',
        backgroundColor: 'rgba(246, 248, 255, 1)',
        borderRadius: 10,
        textAlignVertical: 'center',
        textAlign: 'center',
        marginTop: 21,
    },
    option: {
        width: '100%',
        height: 293,
        marginTop: 12,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
        paddingTop: 18,
        paddingLeft: 39
    },
    opsi: {
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: '500',
        lineHeight: 20,
        letterSpacing: 0,
        marginTop: 37
    },
    about: {
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: '500',
        lineHeight: 20,
        letterSpacing: 0,
    },
    logOut: {
        width: '100%',
        height: 47,
        marginTop: 12,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    out: {
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: '500',
        lineHeight: 20,
        color: 'red'
    },
    outPhoto: {
        width: 19,
        height: 18,
        marginRight: 5
    }
})