import React from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/AntDesign";
import { useSelector } from "react-redux";

const Summary = ({
    navigation,
    route
}) => {
    const { barang } = useSelector((state) => state.barang)

    return (
        <View style={styles.container}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}>
                <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    keyboardVerticalOffset={-500}>
                    <View style={styles.navigasi}>
                        <TouchableOpacity onPress={() => navigation.navigate('Keranjang')}>
                            <Icon
                                name="arrowleft" size={20} color={'black'}
                            />
                        </TouchableOpacity>
                        <Text style={styles.teksJudul}> Summary </Text>
                    </View>
                    <View style={styles.input}>
                        <View style={styles.costumer}>
                            <Text style={styles.data}>Data Costumer</Text>
                            <Text style={styles.bio}>Agil Bani (0813763476)</Text>
                            <Text style={styles.bio}>Jl. Perumnas, Condong catur, Sleman, Yogyakarta</Text>
                            <Text style={styles.bio}>gantengdoang@dipanggang.com</Text>
                        </View>
                        <View style={styles.outlet}>
                            <Text style={styles.judul}>Alamat Outlet Tujuan</Text>
                            <Text style={styles.alamat}>Jack Repair - Seturan (027-343457)</Text>
                            <Text style={styles.alamat}>Jl. Affandi No 18, Sleman, Yogyakarta</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 18}}>
                            <FlatList
                                data={barang}
                                horizontal
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item, index }) => (
                                    <View
                                        style={styles.rekomenTempat}
                                    >
                                        <Image
                                            source={{ uri: item.fotoBarang }}
                                            style={styles.produk}
                                        />
                                        <View
                                            style={styles.deskripsi}
                                        >
                                            <Text style={styles.teks}>{item.merek}-{item.warna}-{item.ukuran}</Text>
                                            <Text style={styles.teks1}>{item.pilihan}</Text>
                                            <Text style={styles.teks1}>Note: {item.catatan}</Text>
                                        </View>
                                    </View>
                                )}
                            />
                    </View>
                    <View style={styles.button}>
                        <TouchableOpacity
                            style={styles.pesanButton}
                            onPress={() => navigation.navigate('Berhasil')}>
                            <Text style={styles.pesanText}>
                                Reservasi Sekarang
                            </Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    )
}

export default Summary

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f8ff',
    },
    arrow: {
        width: 16,
        height: 12,
        resizeMode: 'contain',
    },
    navigasi: {
        width: Dimensions.get("screen").width,
        height: 56,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 25,
        flexDirection: 'row',
        borderBottomWidth: 1,
        shadowColor: '0px 4px 4px rgba(220, 220, 220, 0.25);'
    },
    teksJudul: {
        fontFamily: 'Montserrat',
        fontSize: 18,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 20,
        letterSpacing: 1,
        color: '#201f26',
        marginLeft: 14
    },
    input: {
        width: '100%',
        backgroundColor: '#f6f8ff',
    },
    pesanButton: {
        width: '100%',
        marginTop: 41,
        marginBottom: 44,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    pesanText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    costumer: {
        width: Dimensions.get('screen').width,
        height: 120,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    data: {
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 17,
        letterSpacing: 1,
        color: '#979797',
        marginLeft: 6,
        marginTop: 18
    },
    bio: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 17,
        color: '#201F26',
        marginLeft: 6,
        marginTop: 10
    },
    outlet: {
        width: '100%',
        height: 100,
        marginTop: 12,
        backgroundColor: '#fff',
        paddingHorizontal: 20
    },
    alamat: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 17,
        color: '#201F26',
        marginLeft: 6,
        marginTop: 10
    },
    button: {
        paddingHorizontal: 20
    },
    judul: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 17,
        color: '#979797',
        marginLeft: 6,
        marginTop: 18
    },
    rekomenTempat: {
        width: 365,
        height: 133,
        backgroundColor: '#fff',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginStart: 15,
    },
    produk: {
        height: 84,
        width: 84,
        marginLeft: 14,
        borderRadius: 5
    },
    deskripsi: {
        width: 200,
        height: 80,
        marginLeft: 15,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    teks: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 15,
        letterSpacing: 1,
        color: '#000000',
    },
    teks1: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '400',
        lineHeight: 15,
        letterSpacing: 1,
        color: '#737373',
        marginTop: 11
    },
    barang: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 14,
        lineHeight: 17,
        color: '#979797',
        marginLeft: 27,
    },
})