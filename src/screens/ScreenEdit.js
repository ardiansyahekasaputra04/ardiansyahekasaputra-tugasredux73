import React, { useEffect, useState } from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import Icons from "react-native-vector-icons/AntDesign";
import AsyncStorage from "@react-native-async-storage/async-storage";
import ImageCropPicker from "react-native-image-crop-picker";

const Edit = ({
    navigation,
    route
}) => {
    const [nama, setNama] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [photo, setPhoto] = useState(null);

    const selectPhoto = () => {
        ImageCropPicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            includeBase64: true,
            mediaType: 'photo',
        }).then(image => {
            setPhoto(image);

        }).catch(error => {
            console.log(error);
        });
    };


    const getData = async () => {
        try {
            const retrievedName = await AsyncStorage.getItem('Username');
            const retrievedAddress = await AsyncStorage.getItem('alamat');
            const retrievedPhoto = await AsyncStorage.getItem('photo');
            const retrievedPhoneNumber = await AsyncStorage.getItem('phoneNumber');
            if (retrievedName !== null) {
                setNama(retrievedName);
            }
            if (retrievedPhoneNumber !== null) {
                setPhoneNumber(retrievedPhoneNumber);
            }
            if (retrievedAddress !== null) {
                setAddress(retrievedAddress);
            }
            if (retrievedPhoto !== null) {
                setPhoto(retrievedPhoto ? JSON.parse(retrievedPhoto) : null);
            }
        } catch (error) {
            // Error retrieving data
        }
    };
    useEffect(() => {
        getData();
    }, []);

    const saveData = async () => {
        try {
            await AsyncStorage.setItem('Username', nama);
            await AsyncStorage.setItem('alamat', address);
            await AsyncStorage.setItem('phoneNumber', phoneNumber);
            await AsyncStorage.setItem('photo', photo ? JSON.stringify(photo) : '');
            navigation.navigate('Profil')
            // Display a success message or perform any additional actions after saving the data
        } catch (error) {
            // Error saving data
        }
    };


    return (
        <View
            style={styles.container}
        >
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}
            >
                <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    keyboardVerticalOffset={-500}
                >
                    <View
                        style={styles.navigasi}
                    >
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Profil')}
                        >
                            <Icons
                                name="arrowleft"
                                size={20}
                                color={'black'}
                            />
                        </TouchableOpacity>
                        <Text
                            style={styles.teksJudul}
                        >
                            Edit Profile
                        </Text>
                    </View>
                    <View
                        style={styles.profil}
                    >
                        <Image
                            source={photo && typeof photo === 'object' ? { uri: photo.path } : require('../assets/image/Profil.png')}
                            style={styles.foto}
                        />
                        <TouchableOpacity
                            onPress={selectPhoto}
                            style={{
                                flexDirection: 'row',
                                marginTop: 19
                            }}
                        >
                            <Icon
                                name="pencil-alt"
                                size={20}
                                color={'blue'}
                            />
                            <Text
                                style={styles.email}
                            >
                                Edit Foto
                            </Text>
                        </TouchableOpacity>

                        <View
                            style={styles.input}
                        >
                            <Text
                                style={styles.username}
                            >
                                Nama
                            </Text>
                            <TextInput
                                placeholder="Masukkan Nama"
                                style={styles.usernameInput}
                                value={nama}
                                onChangeText={text => setNama(text)}
                            />
                            <Text
                                style={styles.username}
                            >
                                Alamat
                            </Text>
                            <TextInput
                                placeholder="Masukkan Alamat"
                                style={styles.usernameInput}
                                value={address}
                                onChangeText={text => setAddress(text)}
                            />
                            <Text
                                style={styles.username}
                            >
                                No.Handphone
                            </Text>
                            <TextInput
                                placeholder="Masukkan Nomor"
                                style={styles.usernameInput}
                                value={phoneNumber}
                                onChangeText={text => setPhoneNumber(text)}
                            />
                            <TouchableOpacity
                                style={styles.loginButton}
                                onPress={saveData}
                            >
                                <Text
                                    style={styles.loginText}
                                >
                                    Done
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    )
}

export default Edit

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f8ff',
    },
    profil: {
        width: Dimensions.get('screen').width,
        height: 656,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: '#fff',
        paddingTop: 31
    },
    foto: {
        width: 95,
        height: 95
    },
    edit: {
        width: 24,
        height: 24,
    },
    email: {
        fontFamily: 'Montserrat',
        fontSize: 18,
        fontWeight: '500',
        letterSpacing: 0,
        textAlign: 'left',
        color: '#3A4BE0',
        marginLeft: 9
    },
    Bukabck: {
        width: 64,
        height: 27,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 17,
        letterSpacing: 1,
        color: '#050152',
        backgroundColor: 'rgba(246, 248, 255, 1)',
        borderRadius: 10,
        textAlignVertical: 'center',
        textAlign: 'center',
        marginTop: 21,
    },
    option: {
        width: '100%',
        height: 293,
        marginTop: 12,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
        paddingTop: 18,
        paddingLeft: 39
    },
    opsi: {
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: '500',
        lineHeight: 20,
        letterSpacing: 0,
        marginTop: 37
    },
    about: {
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: '500',
        lineHeight: 20,
        letterSpacing: 0,
    },
    logOut: {
        width: '100%',
        height: 47,
        marginTop: 12,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    out: {
        fontFamily: 'Montserrat',
        fontSize: 16,
        fontWeight: '500',
        lineHeight: 20,
        color: 'red'
    },
    outPhoto: {
        width: 19,
        height: 18,
        marginRight: 5
    },
    arrow: {
        width: 16,
        height: 12,
        resizeMode: 'contain',
    },
    navigasi: {
        width: Dimensions.get("screen").width,
        height: 56,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 25,
        flexDirection: 'row',
        borderBottomWidth: 1,
        shadowColor: '0px 4px 4px rgba(220, 220, 220, 0.25);'
    },
    teksJudul: {
        fontFamily: 'Montserrat',
        fontSize: 18,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 20,
        letterSpacing: 1,
        color: '#201f26',
        marginLeft: 14
    },
    input: {
        width: '100%',
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        marginTop: 55
    },
    usernameInput: {
        marginTop: 11,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 12,
        textAlignVertical: 'center'
    },
    username: {
        color: 'red',
        fontWeight: 'bold',
        marginTop: 27
    },
    loginButton: {
        width: '100%',
        marginTop: 77,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    loginText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
})