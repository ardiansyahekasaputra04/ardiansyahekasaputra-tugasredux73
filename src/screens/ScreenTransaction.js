import moment from "moment";
import React from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground,
    ViewBase
} from "react-native";

const Transaction = ({
    navigation,
    route
}) => {
    return (
        <View style={styles.container}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}>
                <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    keyboardVerticalOffset={-500}>
                    <View style={styles.navigasi}>
                        <TouchableOpacity onPress={() => navigation.navigate('Transaction')}>
                            <Image
                                source={require('../assets/icon/Blackarrow.png')}
                                style={styles.arrow} />
                        </TouchableOpacity>
                        <Text style={styles.teksJudul}>Transaksi</Text>
                    </View>
                    <TouchableOpacity onPress={() => navigation.navigate('Reservasi')}>
                        <View style={styles.tampilan}>
                            <View style={styles.rekomenTempat}>
                                <Text style={styles.tanggal}>{moment().format('DD MMMM YYYY HH:mm')}</Text>
                                <Text style={styles.produk}>New Balance - Pink Abu - 40</Text>
                                <Text style={styles.jenis}>Cuci Sepatu</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.namaKode}>Kode Reservasi :  <Text style={styles.kode}>CS{moment().format('MMYYYY')}</Text></Text>
                                    <Text style={styles.Bukabck}> Reserved </Text>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    )
}

export default Transaction

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f8ff',
    },
    arrow: {
        width: 16,
        height: 12,
        resizeMode: 'contain',
    },
    navigasi: {
        width: Dimensions.get("screen").width,
        height: 56,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 25,
        flexDirection: 'row',
        borderBottomWidth: 1,
        shadowColor: '0px 4px 4px rgba(220, 220, 220, 0.25);'
    },
    teksJudul: {
        fontFamily: 'Montserrat',
        fontSize: 18,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 20,
        letterSpacing: 1,
        color: '#201f26',
        marginLeft: 14
    },
    rekomenTempat: {
        width: '100%',
        height: 128,
        marginTop: 25,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
        marginBottom: 43,
    },
    produk: {
        height: 84,
        width: 84,
        marginLeft: 14,
        borderRadius: 5
    },
    deskripsi: {
        width: 200,
        height: 80,
        marginLeft: 15,
        backgroundColor: '#ffffff',
        borderRadius: 9,
        justifyContent: 'flex-start',
        flexDirection: 'column'
    },
    teks: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '500',
        lineHeight: 15,
        letterSpacing: 1,
        color: '#000000',
    },
    teks1: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '400',
        lineHeight: 15,
        letterSpacing: 1,
        color: '#737373',
        marginTop: 11
    },
    tampilan: {
        paddingHorizontal: 20
    },
    tanggal: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 15,
        textAlign: 'center',
        marginTop: 18,
        marginLeft: 10,
        color: '#BDBDBD',
    },
    produk: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 15,
        textAlign: 'center',
        marginTop: 13,
        marginLeft: 10,
        color: '#201F26',
    },
    jenis: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 15,
        textAlign: 'center',
        marginTop: 3,
        marginLeft: 10,
        color: '#201F26',
    },
    namaKode: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontWeight: '400',
        lineHeight: 15,
        textAlign: 'center',
        marginTop: 13,
        marginLeft: 10,
        color: '#201F26',
    },
    kode: {
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontWeight: '700',
        lineHeight: 15,
        textAlign: 'center',
        marginTop: 13,
        color: '#201F26',
    },
    Bukabck: {
        width: 81,
        height: 21,
        fontFamily: 'Montserrat',
        fontSize: 12,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 17,
        letterSpacing: 1,
        color: '#FFC107',
        backgroundColor: 'rgba(242, 156, 31, 0.16)',
        borderRadius: 10,
        textAlignVertical: 'center',
        textAlign: 'center',
        marginTop: 10,
        marginLeft: 95
    },
})