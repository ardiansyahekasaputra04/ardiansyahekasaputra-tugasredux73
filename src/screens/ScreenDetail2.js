import React from "react";
import {
   View,
   Text,
   ScrollView,
   KeyboardAvoidingView,
   Image,
   TextInput,
   TouchableOpacity,
   StyleSheet,
   Dimensions,
   ImageBackground
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import Icons from 'react-native-vector-icons/Feather'
import Iconss from 'react-native-vector-icons/Entypo'

const Detail2 = ({
   navigation,
   route
}) => {
   return (
      <View style={styles.container}>
         <ScrollView //component yang digunakan agar tampilan kita bisa discroll
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: 10 }}
         >
            <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
               behavior='padding' //tampilan form atau text input
               enabled
               keyboardVerticalOffset={-500}
            >
               <ImageBackground
                  source={require('../assets/image/Seturan.png')} //load atau panggil asset image dari local
                  style={styles.background}>
                  <View
                     style={styles.arrowBag}>
                     <TouchableOpacity onPress={() => navigation.navigate('BottomTab')}>
                        <Icon
                           name="arrowleft" size={20} color={'black'}
                        />
                     </TouchableOpacity>
                     <Icons
                        name="shopping-bag" size={30} color={'white'}
                     />
                  </View>
               </ImageBackground>
               <View style={styles.review}>
                  <Text style={styles.namaTempat}> Jack Repair Seturan </Text>
                  <View style={{ flexDirection: 'row',marginLeft: 20}}>
                     <Icon
                        name="star" size={13} color={'yellow'}
                     />
                     <Icon
                        name="star" size={13} color={'yellow'}
                     />
                     <Icon
                        name="star" size={13} color={'yellow'}
                     />
                     <Icon
                        name="star" size={13} color={'yellow'}
                     />
                     <Icon
                        name="staro" size={13} color={'yellow'}
                     />
                  </View>
                  <View style={styles.alamat}>
                     <Iconss name="location-pin" size={20} color={'red'}/>
                     <View style={{ flexDirection: 'column' }}>
                        <Text style={styles.teksAlamat}> Jalan Affandi (Gejayan), No.15, Sleman </Text>
                        <Text style={styles.teksAlamat}> Yogyakarta, 55384 </Text>
                     </View>
                     <TouchableOpacity>
                        <Text style={styles.maps}> Lihat Maps </Text>
                     </TouchableOpacity>
                  </View>
                  <View style={styles.jadwal}>
                     <Text style={styles.Bukabck}> Buka </Text>
                     <Text style={styles.jambuka}> 09.00-21.00</Text>
                  </View>
                  <View style={styles.deskripsi}>
                     <Text style={styles.judulDeskripsi}> Deskripsi </Text>
                     <Text style={styles.kalimatDeskripsi}> Lorem ipsum dolor sit amet, consectetur adipiscing </Text>
                     <Text style={styles.kalimatDeskripsi}> elit. Massa gravida mattis arcu interdum lectus </Text>
                     <Text style={styles.kalimatDeskripsi}> egestas scelerisque. Blandit porttitor diam viverra </Text>
                     <Text style={styles.kalimatDeskripsi}> amet nulla sodales aliquet est. Donec enim turpis </Text>
                     <Text style={styles.kalimatDeskripsi}> rhoncus quis integer. Ullamcorper morbi donec </Text>
                     <Text style={styles.kalimatDeskripsi}> tristique condimentum ornare imperdiet facilisi </Text>
                     <Text style={styles.kalimatDeskripsi}> pretium molestie. </Text>
                     <Text style={styles.judulDeskripsi}> Range Biaya </Text>
                     <Text style={styles.Harga}> Rp 20.000-80.000,- </Text>
                  </View>
                  <View style={{marginHorizontal: 20}}>
                     <TouchableOpacity
                        style={styles.loginButton}
                        onPress={() => navigation.navigate('Pemesanan')}
                     >
                        <Text style={styles.loginText}>
                           Repair disini
                        </Text>
                     </TouchableOpacity>
                  </View>
               </View>
            </KeyboardAvoidingView>
         </ScrollView>
      </View>
   )
}

export default Detail2

const styles = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: '#fff',
   },
   background: {
      width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
      height: 317,
   },
   review: {
      width: Dimensions.get('window').width,
      backgroundColor: '#fff',
      borderRadius: 19,
      paddingTop: 38,
      marginTop: -20
   },
   namaTempat: {
      height: 20,
      fontFamily: 'Montserrat',
      fontSize: 18,
      fontStyle: 'normal',
      fontWeight: '700',
      lineHeight: 17,
      letterSpacing: 1,
      color: '#201f26',
      marginLeft: 17
   },
   rating: {
      width: 66,
      height: 11,
      marginBottom: 15,
      marginLeft: 20
   },
   alamat: {
      width: 3 - 12,
      height: 37,
      backgroundColor: '#ffffff',
      borderRadius: 9,
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      marginBottom: 7,
      marginLeft: 20
   },
   gambarLokasi: {
      width: 14,
      height: 18
   },
   teksAlamat: {
      fontFamily: 'Montserrat',
      fontSize: 10,
      fontStyle: 'normal',
      fontWeight: '400',
      lineHeight: 17,
      letterSpacing: 1,
      color: '#979797'
   },
   maps: {
      fontFamily: 'Montserrat',
      fontSize: 15,
      fontStyle: 'normal',
      fontWeight: '700',
      lineHeight: 17,
      letterSpacing: 1,
      color: '#3471cd',
      marginRight: 5
   },
   jadwal: {
      paddingLeft: 2,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      paddingRight: 41,
      width: '100%',
      paddingBottom: 17,
      marginLeft: 15
   },
   Bukabck: {
      width: 58,
      height: 21,
      fontFamily: 'Montserrat',
      fontSize: 12,
      fontStyle: 'normal',
      fontWeight: '700',
      lineHeight: 17,
      letterSpacing: 1,
      color: '#11a84e',
      backgroundColor: 'rgba(17, 168, 78, 0.12)',
      borderRadius: 10,
      textAlignVertical: 'center',
      textAlign: 'center',
      marginRight: 13,
   },
   jambuka: {
      fontFamily: 'Montserrat',
      fontSize: 15,
      fontStyle: 'normal',
      fontWeight: '700',
      lineHeight: 24,
      letterSpacing: 1,
      color: '#3471cd',
   },
   deskripsi: {
      width: Dimensions.get('window').width,
      backgroundColor: '#fff',
      paddingRight: 20,
      borderTopWidth: 2
   },
   judulDeskripsi: {
      fontFamily: 'Montserrat',
      fontSize: 16,
      fontStyle: 'normal',
      fontWeight: '500',
      lineHeight: 17,
      color: '#201f26',
      marginLeft: 20,
      paddingBottom: 10,
      paddingTop: 23
   },
   kalimatDeskripsi: {
      fontFamily: 'Montserrat',
      fontSize: 13,
      fontStyle: 'normal',
      fontWeight: '400',
      lineHeight: 17,
      color: '#595959',
      marginLeft: 20,
      width: 313
   },
   Harga: {
      fontFamily: 'Montserrat',
      fontSize: 16,
      fontStyle: 'normal',
      fontWeight: '500',
      lineHeight: 17,
      letterSpacing: 1,
      color: '#8d8d8d',
      marginLeft: 20,
      width: 313,
      marginBottom: 4
   },
   loginButton: {
      width: '100%',
      marginTop: 77,
      marginBottom: 34,
      backgroundColor: '#BB2427',
      borderRadius: 8,
      paddingVertical: 15,
      justifyContent: 'center',
      alignItems: 'center',
   },
   loginText: {
      color: '#fff',
      fontSize: 16,
      fontWeight: 'bold'
   },
   arrow: {
      width: 16,
      height: 12,
      resizeMode: 'contain',
   },
   arrowBag: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 28,
      justifyContent: 'space-between',
      paddingLeft: 30,
      paddingRight: 34,
   },
   bag: {
      width: 18,
      height: 20,
      resizeMode: 'contain',
   },
})