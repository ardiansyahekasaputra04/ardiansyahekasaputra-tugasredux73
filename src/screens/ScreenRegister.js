// import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useState } from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    Alert
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import { useDispatch, useSelector } from "react-redux";

const Register = ({
    navigation,
    route
}) => {
    const { account } = useSelector((state) => state.account)
    const dispatch = useDispatch()
    const [nama, setNama] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirm, setConfirm] = useState('')
    const AddAccount = async () => {
        if (nama && email && password && confirm == '') {
            Alert.alert('Peringatan', 'Isi data terlebih dahulu')
        }
        if (nama == '') {
            Alert.alert('Peringatan', 'Nama belum terisi')
        }
        if (email == '') {
            Alert.alert('Peringatan', 'Email belum terisi')
        }
        if (password == '') {
            Alert.alert('Peringatan', 'Password belum terisi')
        }
        if (confirm == '') {
            Alert.alert('Peringatan', 'Konfirmasi password belum terisi')
        }
        if (confirm !== password) {
            Alert.alert('Peringatan', 'Konfirmasi Salah')
        }
        var dataAccount = [...account]
        const data = {
            nama: nama,
            email: email,
            password: password,
            confirm: confirm
        }
        dataAccount.push(data)
        dispatch({
            type: 'ADD_ACCOUNT',
            data: dataAccount
        })
        navigation.navigate(
            'Login'
        )
    }

    const checkData = () => {
        if (route.params) {
            const data = route.params.item;
            setNama(data.nama)
            setEmail(data.email);
            setPassword(data.password);
            setConfirm(data.confirm);
        }
    };
    React.useEffect(() => {
        checkData();
    }, []);
    // const storedNama = async (nama) => {
    //     try {
    //         await AsyncStorage.setItem('Username', nama)
    //     } catch (errror) {
    //         console.log('error')
    //     }
    // }
    // const storedEmail = async (email) => {
    //     try {
    //         await AsyncStorage.setItem('Email', email)
    //     } catch (errror) {
    //         console.log('error')
    //     }
    // }
    // const storedPassword = async (password) => {
    //     try {
    //         await AsyncStorage.setItem('Password', password)
    //     } catch (errror) {
    //         console.log('error')
    //     }
    // }
    // const storedConfirm = async (confirm) => {
    //     try {
    //         await AsyncStorage.setItem('Confirm', confirm)
    //     } catch (errror) {
    //         console.log('error')
    //     }
    // }
    // const setData = async () => {
    //     const data = {
    //         nama,
    //         email,
    //         password,
    //         confirm
    //     }
    //     if (data.nama == 0 && data.email == 0 && data.password == 0 && data.confirm == 0) {
    //         Alert.alert('Peringatan', 'Isi data terlebih dahulu')
    //     } else {
    //         if (data.nama == 0) {
    //             Alert.alert('Peringatan', 'Nama belum terisi')
    //         } else {
    //             if (data.email == 0) {
    //                 Alert.alert('Peringatan', 'Email belum terisi')
    //             } else {
    //                 if (data.password == 0) {
    //                     Alert.alert('Peringatan', 'Password belum terisi')
    //                 } else {
    //                     if (data.confirm == 0) {
    //                         Alert.alert('Peringatan', 'Konfirmasi password belum terisi')
    //                     } else {
    //                         if (data.confirm !== data.password) {
    //                             Alert.alert('Peringatan', 'Konfirmasi Salah')
    //                         } else {
    //                             await storedNama(nama)
    //                             await storedEmail(email)
    //                             await storedPassword(password)
    //                             await storedConfirm(confirm)
    //                             navigation.navigate('Login')
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    return (
        <View
            style={styles.container}
        >
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}
            >
                <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    keyboardVerticalOffset={-500}
                >
                    <Image
                        source={require('../assets/image/LoginBackground.png')} //load atau panggil asset image dari local
                        style={styles.background}
                    />
                    <View style={styles.input}>
                        <Text
                            style={styles.title1}>
                            Welcome,
                        </Text>
                        <Text
                            style={styles.title2}>
                            Please Register
                        </Text>
                        <Text
                            style={styles.username}>
                            Username
                        </Text>
                        <TextInput
                            placeholder="Masukkan Username"
                            style={styles.usernameInput}
                            onChangeText={(text) => setNama(text)}
                        />
                        <Text
                            style={styles.email}
                        >
                            Email
                        </Text>
                        <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                            caretHidden={false}
                            placeholder='Masukkan Email' //pada tampilan ini, kita ingin user memasukkan email
                            style={styles.emailInput}
                            onChangeText={(text) => setEmail(text)}
                            keyboardType="email-address" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
                        />
                        <Text
                            style={styles.password}
                        >
                            Password
                        </Text>
                        <TextInput //component yang digunakan untuk memasukkan data password
                            placeholder='Masukkan Password'
                            secureTextEntry={true} //props yang digunakan untuk menyembunyikan password user
                            style={styles.passwordInput}
                            onChangeText={(text) => setPassword(text)}
                        />
                        <Text
                            style={styles.password}
                        >
                            Confrim Password
                        </Text>
                        <TextInput //component yang digunakan untuk memasukkan data password
                            placeholder='Masukkan Konfirmasi'
                            secureTextEntry={true} //props yang digunakan untuk menyembunyikan password user
                            style={styles.passwordInput}
                            onChangeText={(text) => setConfirm(text)}
                        />
                        <View
                            style={styles.backgroundButton}
                        >
                            <View
                                style={styles.buttonLogo}
                            >
                                <TouchableOpacity onPress={() => navigation.navigate('')}>
                                    <Icon
                                        name="google" //load asset dari local
                                        size={30}
                                        color={'red'}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Icon name="facebook-square"
                                        size={30}
                                        color={'blue'}
                                        style={{ marginHorizontal: 15 }}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <Icon name='twitter'
                                        size={30}
                                        color={'aqua'}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <TouchableOpacity
                            style={styles.registerButton}
                            onPress={AddAccount}
                        >
                            <Text
                                style={styles.registerText}
                            >
                                Register
                            </Text>
                        </TouchableOpacity>
                        <View
                            style={styles.loginNavigate}
                        >
                            <Text
                                style={styles.loginQuestion}
                            >
                                Have An Account yet?
                            </Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Login')}
                            >
                                <Text
                                    style={styles.loginText}
                                >
                                    Login
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    background: {
        width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
        height: 317,
    },
    title1: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#0A0827'
    },
    title2: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#0A0827'
    },
    input: {
        width: '100%',
        backgroundColor: '#fff',
        borderTopLeftRadius: 19,
        borderTopRightRadius: 19,
        paddingHorizontal: 20,
        paddingTop: 38,
        marginTop: -20
    },
    username: {
        color: 'red',
        fontWeight: 'bold',
        marginTop: 25
    },
    usernameInput: {
        marginTop: 11,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 10
    },
    email: {
        color: 'red',
        fontWeight: 'bold',
        marginTop: 11
    },
    emailInput: {
        marginTop: 11,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 10
    },
    password: {
        color: 'red',
        fontWeight: 'bold',
        marginTop: 11,
    },
    passwordInput: {
        marginTop: 11,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 10
    },
    backgroundButton: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 15,
        justifyContent: 'space-between',
    },
    buttonLogo: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    gmail: {
        width: 36,
        height: 25,
        resizeMode: 'contain'
    },
    facebook: {
        width: 36,
        height: 25,
        marginHorizontal: 15,
        resizeMode: 'contain'
    },
    twitter: {
        width: 36,
        height: 25,
        resizeMode: 'contain'
    },
    registerButton: {
        width: '100%',
        marginTop: 77,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    registerText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    loginNavigate: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 39,
        marginBottom: 23,
        flexDirection: 'row',
    },
    loginQuestion: {
        fontSize: 12,
        color: '#717171'
    },
    loginText: {
        fontSize: 14,
        color: '#BB2427',
        marginLeft: 5
    },
});