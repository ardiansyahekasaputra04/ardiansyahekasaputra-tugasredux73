import React from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
    ImageBackground
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

const Berhasil = ({
    navigation,
    route
}) => {
    return (
        <View
            style={styles.container}
        >
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}
            >
                <TouchableOpacity
                    onPress={() => navigation.navigate('BottomTab')}
                >
                    <Icon
                        name="close"
                        size={30}
                        color={'black'}
                        style={{ paddingTop: 15, paddingLeft: 15 }}
                    />
                </TouchableOpacity>
                <View
                    style={styles.utama}
                >
                    <Text
                        style={styles.judul}
                    >
                        Reservasi Berhasil
                    </Text>
                    <View
                        style={styles.centang}
                    >
                        <Icon
                            name="check"
                            size={134}
                            color={'green'}
                        />
                    </View>
                    <Text
                        style={styles.deskripsi}
                    >
                        Kami Telah Mengirimkan Kode
                    </Text>
                    <Text
                        style={styles.deskripsi}
                    >
                        Reservasi ke Menu Transaksi
                    </Text>
                </View>
                <View
                    style={styles.button}
                >
                    <TouchableOpacity
                        style={styles.pesanButton}
                        onPress={() => navigation.navigate('Reservasi')}
                    >
                        <Text style={styles.pesanText}>
                            Lihat Kode Reservasi
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

export default Berhasil

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f6f8ff',
    },
    utama: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 176
    },
    judul: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '700',
        fontSize: 18,
        lineHeight: 17,
        letterSpacing: 0.257143,
        color: '#11A84E',
    },
    centang: {
        width: 175,
        height: 175,
        marginTop: 54,
        marginBottom: 68,
        backgroundColor: 'rgba(17, 168, 78, 0.5)',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 32
    },
    deskripsi: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 18,
        lineheight: 17,
        textAlign: 'center',
        letterSpacing: 0.257143,
        color: '#000000',
    },
    pesanButton: {
        width: '100%',
        marginTop: 41,
        marginBottom: 44,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    pesanText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    button: {
        paddingHorizontal: 20,
        marginTop: 100
    },
})