import moment from "moment"
import React, { useEffect, useState } from "react"
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions,
} from "react-native";
import ImageCropPicker from "react-native-image-crop-picker";
import Icon from "react-native-vector-icons/AntDesign";
import Icons from "react-native-vector-icons/Entypo";
import { useDispatch, useSelector } from "react-redux"

const Pemesanan = ({navigation, route}) => {
    const { barang } = useSelector((state) => state.barang)
    const dispatch = useDispatch()
    const [idBarang, setIdBarang] = useState(moment(new Date).format('YYMMDDHHmmss'))
    const [merek, setMerek] = useState('')
    const [warna, setWarna] = useState('')
    const [ukuran, setUkuran] = useState(0)
    const [fotoBarang, setFotoBarang] = useState('')
    const [pilihan, setPilihan] = useState('')
    const [catatan, setCatatan] = useState('')

    //Foto
    const selectStoreImage = () => {
        ImageCropPicker.openPicker({
            cropping: true,
            includeBase64: true,
            compressImageQuality: 0.8,
            freeStyleCropEnabled: true,
            cropperStatusBarColor: 'white',
            cropperToolbarColor: 'white',
            cropperToolbarTitle: 'Crop Image',
            cropperToolbarWidgetColor: '#2f80ed',
        })
            .then((image) => {
                setFotoBarang(image.path);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    //Utama
    const AddBarang = async () => {
        if (!idBarang || !merek || !warna || !ukuran || !fotoBarang || !pilihan || !catatan) {
            alert('Semua kolom harus diisi!');
            return;
        }
        if (ukuran == 0) {
            alert('Tulis ukuran kaki baumu dulu :v')
            return
        }
        var dataBarang = [...barang]
        const data = {
            id: idBarang,
            merek: merek,
            warna: warna,
            ukuran: ukuran,
            fotoBarang: fotoBarang,
            pilihan: pilihan,
            catatan: catatan
        }
        dataBarang.push(data)
        dispatch({ type: 'ADD_BARANG', data: dataBarang })
        navigation.navigate('Keranjang')
        // console.log("ADD", data)
    }
    const checkData = () => {
        if (route.params) {
            const data = route.params.item;
            setIdBarang(data.id)
            setMerek(data.merek)
            setWarna(data.warna)
            setUkuran(data.ukuran)
            setFotoBarang(data.fotoBarang)
            setPilihan(data.pilihan)
            setCatatan(data.catatan)
        }
    }
    useEffect(() => {
        checkData()
    }, [])
    const UpdateBarang = async () => {
        const data = {
            id: idBarang,
            merek: merek,
            warna: warna,
            ukuran: ukuran,
            fotoBarang: fotoBarang,
            pilihan: pilihan,
            catatan: catatan
        }
        dispatch({ type: 'UPDATE_BARANG', data });
        navigation.navigate('Keranjang');
        console.log('UPDATE', data)
    }
    const DeleteBarang = async () => {
        dispatch({ type: 'DELETE_BARANG', id: idBarang })
        navigation.navigate('Keranjang')
        console.log('DELETE', id)
    }

    return (
        <View style={styles.container}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}>
                <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    keyboardVerticalOffset={-500}>
                    <View style={styles.navigasi}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Icon
                                name="arrowleft" size={20} color={'black'}
                            />
                        </TouchableOpacity>
                        <Text style={styles.teksJudul}> Form Pemesanan </Text>
                    </View>
                    <View style={styles.input}>
                        <Text style={styles.username}>Merek</Text>
                        <TextInput
                            placeholder="Masukkan Merek Barang"
                            style={styles.usernameInput}
                            value={merek}
                            onChangeText={setMerek}
                            keyboardType="default" />
                        <Text style={styles.username}>Warna</Text>
                        <TextInput
                            placeholder="Warna Barang, cth : Merah - Putih "
                            style={styles.usernameInput}
                            value={warna}
                            onChangeText={setWarna}
                            keyboardType='default' />
                        <Text style={styles.username}>Ukuran</Text>
                        <TextInput
                            placeholder="Cth : 39,40 "
                            style={styles.usernameInput}
                            value={ukuran}
                            onChangeText={setUkuran}
                            keyboardType='numeric' />
                        <Text style={styles.username}>Photo</Text>
                        <TouchableOpacity onPress={selectStoreImage}>
                            {fotoBarang ? (
                                <View style={styles.selectImageButton}>
                                    <Image source={{ uri: fotoBarang }} style={styles.storeImage} />
                                </View>
                            ) : (
                                <View style={styles.foto}>
                                    <Icon name="camera" size={30} color={'red'} />
                                    <Text style={styles.addPhoto}>Add Photo</Text>
                                </View>
                            )}
                        </TouchableOpacity>
                        <View style={styles.checkList}>
                            <View style={styles.checkList1}>
                            <TouchableOpacity
                                    onPress={() => setPilihan('Ganti Sol Sepatu')}
                                    style={[styles.checkBox, {
                                        backgroundColor: pilihan == 'Ganti Sol Sepatu' ? 'red' : 'white',
                                    }]}
                                >
                                    {pilihan == 'Ganti Sol Sepatu' ? (
                                        <Icon name="check" size={20} color="white" />
                                    ) : null}
                                </TouchableOpacity>
                                <Text style={styles.checkText}>Ganti Sol Sepatu</Text>
                            </View>
                            <View style={styles.checkList1}>
                            <TouchableOpacity
                                    onPress={() => setPilihan('Jahit Sepatu')}
                                    style={[styles.checkBox, {
                                        backgroundColor: pilihan == 'Jahit Sepatu' ? 'red' : 'white',
                                    }]}
                                >
                                    {pilihan == 'Jahit Sepatu' ? (
                                        <Icon name="check" size={20} color="white" />
                                    ) : null}
                                </TouchableOpacity>
                                <Text style={styles.checkText}>Jahit Sepatu</Text>
                            </View>
                            <View style={styles.checkList1}>
                                <TouchableOpacity
                                    onPress={() => setPilihan('Repaint Sepatu')}
                                    style={[styles.checkBox, {
                                        backgroundColor: pilihan == 'Repaint Sepatu' ? 'red' : 'white',
                                    }]}
                                >
                                    {pilihan == 'Repaint Sepatu' ? (
                                        <Icon name="check" size={20} color="white" />
                                    ) : null}
                                </TouchableOpacity>
                                <Text style={styles.checkText}>Repaint Sepatu</Text>
                            </View>
                            <View style={styles.checkList1}>
                                <TouchableOpacity
                                    onPress={() => setPilihan('Cuci Sepatu')}
                                    style={[styles.checkBox, {
                                        backgroundColor: pilihan == 'Cuci Sepatu' ? 'red' : 'white',
                                    }]}
                                >
                                    {pilihan == 'Cuci Sepatu' ? (
                                        <Icon name="check" size={20} color="white" />
                                    ) : null}
                                </TouchableOpacity>
                                <Text style={styles.checkText}>Cuci Sepatu</Text>
                            </View>
                        </View>
                        <View style={styles.catatan}>
                            <Text style={styles.catatanTeks}>Catatan</Text>
                            <TextInput
                                placeholder="Cth : S, M, L / 39,40 "
                                multiline
                                value={catatan}
                                onChangeText={setCatatan}
                                keyboardType="default"
                                style={styles.catatanInput} />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                style={styles.pesanButton}
                                onPress={() => {
                                    if (route.params) {
                                        UpdateBarang()
                                    } else {
                                        AddBarang()
                                    }
                                }}
                            >
                                <Text style={styles.pesanText}>
                                    {route.params ? 'Ubah' : 'Tambah'}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.delete} onPress={DeleteBarang}>
                                <Icons
                                    name="trash"
                                    size={20}
                                    color='black'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    arrow: {
        width: 16,
        height: 12,
        resizeMode: 'contain',
    },
    navigasi: {
        width: Dimensions.get("screen").width,
        height: 56,
        backgroundColor: '#fff',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 25,
        flexDirection: 'row',
        borderBottomWidth: 1,
        shadowColor: '0px 4px 4px rgba(220, 220, 220, 0.25);'
    },
    teksJudul: {
        fontFamily: 'Montserrat',
        fontSize: 18,
        fontStyle: 'normal',
        fontWeight: '700',
        lineHeight: 20,
        letterSpacing: 1,
        color: '#201f26',
        marginLeft: 14
    },
    input: {
        width: '100%',
        backgroundColor: '#fff',
        paddingHorizontal: 20,
    },
    usernameInput: {
        marginTop: 11,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 12,
        textAlignVertical: 'center'
    },
    username: {
        color: 'red',
        fontWeight: 'bold',
        marginTop: 27
    },
    kamera: {
        width: 20,
        height: 18
    },
    addPhoto: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 12,
        lineHeight: 15,
        textAlign: 'center',
        color: 'red',
        paddingTop: 15
    },
    checkList: {
        width: 181,
    },
    checkList1: {
        width: 181,
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 23,
    },
    checkBox: {
        width: 24,
        height: 24,
        borderWidth: 1,
        borderRadius: 3,
        marginRight: 23
    },
    checkText: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 17,
        letterSpacing: 1,
    },
    catatan: {
        width: '100%',
        backgroundColor: '#fff',
    },
    catatanInput: {
        marginTop: 11,
        width: '100%',
        height: 92,
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 12,
        textAlignVertical: 'top'
    },
    catatanTeks: {
        color: 'red',
        fontWeight: 'bold',
        marginTop: 27
    },
    pesanButton: {
        width: '81%',
        marginTop: 41,
        marginBottom: 44,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        marginRight: 10,
        alignItems: 'center'
    },
    pesanText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    foto: {
        width: 84,
        height: 84,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        borderWidth: 1,
        borderColor: 'red',
        borderRadius: 8,
        marginTop: 12,
        marginBottom: 46
    },
    selectImageButton: {
        marginRight: 10,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: 'red',
        width: 120,
        height: 120,
        justifyContent: 'center',
        alignItems: 'center',
    },
    storeImage: {
        width: 100,
        height: 100,
    },
    delete: {
        width: 50,
        height: 50,
        marginTop: 41,
        marginBottom: 44,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default Pemesanan