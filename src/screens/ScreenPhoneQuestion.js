import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useState } from "react";
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    Dimensions
} from "react-native";

const PhoneQuestion = ({ navigation, route }) => {
    const [phoneNumber, setPhoneNumber] = useState('');

    const storedPhoneNumber = async (phoneNumber) => {
        try {
            await AsyncStorage.setItem('phoneNumber', phoneNumber)
        } catch (errror) {
            console.log('error')
        }
    }

    const setData = async () => {
        if (phoneNumber == 0) {
            Alert.alert('Peringatan', 'Nama belum terisi')
        } else {
            await storedPhoneNumber(phoneNumber)
            navigation.navigate('BottomTab')
        }
    }

    return (
        <View style={styles.container}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}
            >
                <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
                    behavior='padding' //tampilan form atau text input
                    enabled
                    keyboardVerticalOffset={-500}
                >
                    <Image
                        source={require('../assets/image/LoginBackground.png')} //load atau panggil asset image dari local
                        style={styles.background}
                    />
                    <View style={styles.input}>
                        <Text style={styles.title}>Do You Want To Add Phone Number?</Text>
                        <TextInput
                            placeholder="Masukkan Nomor HP"
                            style={styles.phoneInput}
                            onChangeText={text => setPhoneNumber(text)}
                        />
                        <TouchableOpacity
                            style={styles.nextButton}
                            onPress={setData}
                        >
                            <Text style={styles.nextText}>
                                Next
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.nextNavigate}>
                        <Text style={styles.nextQuestion}>
                            No, Thank You
                        </Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                            <Text style={styles.nextText2}>
                                Next
                            </Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </View>
    )
}

export default PhoneQuestion

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    background: {
        width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
        height: 317,
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#0A0827'
    },
    input: {
        width: '100%',
        backgroundColor: '#fff',
        borderTopLeftRadius: 19,
        borderTopRightRadius: 19,
        paddingHorizontal: 20,
        paddingTop: 38,
        marginTop: -20
    },
    phoneInput: {
        marginTop: 11,
        width: '100%',
        borderRadius: 8,
        backgroundColor: '#F6F8FF',
        paddingHorizontal: 10
    },
    nextButton: {
        width: '100%',
        marginTop: 150,
        backgroundColor: '#BB2427',
        borderRadius: 8,
        paddingVertical: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nextText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    nextNavigate: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        flexDirection: 'row',
    },
    nextQuestion: {
        fontSize: 12,
        color: '#717171'
    },
    nextText2: {
        fontSize: 14,
        color: '#BB2427',
        marginLeft: 5
    },
});