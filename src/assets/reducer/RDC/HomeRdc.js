const initialState = {
    toko: []
}

const homeReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TOKO':
            return {
                ...state,
                toko: action.data,
            };
        case 'UPDATE_TOKO':
            var newData = [...state.toko];
            var findIndex = state.toko.findIndex((value) => { return value.id === action.data.id })

            newData[findIndex] = action.data
            return {
                ...state,
                toko: newData
            }
        case 'DELETE_TOKO':
            var newData = [...state.toko];
            var findIndex = state.toko.findIndex((value) => { return value.id === action.id })

            newData.splice(findIndex, 1)
            return {
                ...state,
                toko: newData
            }
        default:
            return state;
    }
}
export default homeReducer;