const initialState = {
    account: {
        nama: '',
        email: '',
        username: '',
        access_token: '',
    },
    isLoggedIn: false,

}

const accountReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                isLoggedIn: true,
                account: action.data,
            };
        case 'LOGOUT':
            return {
                ...state,
                account: {
                    nama: '',
                    email: '',
                    username: '',
                    access_token: '',
                },
                isLoggedIn: false,
            };

        default:
            return state;
    }
}
export default accountReducer;