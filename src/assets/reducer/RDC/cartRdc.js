const initialState = {
    barang: []
}

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_BARANG':
            return {
                ...state,
                barang: action.data,
            };
        case 'UPDATE_BARANG':
            var newData = [...state.barang];
            var findIndex = state.barang.findIndex((value) => { return value.id === action.data.id })

            newData[findIndex] = action.data
            return {
                ...state,
                barang: newData
            }
        case 'DELETE_BARANG':
            var newData = [...state.barang];
            var findIndex = state.barang.findIndex((value) => { return value.id === action.id })

            newData.splice(findIndex, 1)
            return {
                ...state,
                barang: newData
            }
        default:
            return state;
    }
}
export default cartReducer;