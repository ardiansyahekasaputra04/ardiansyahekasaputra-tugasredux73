export const loginSuccess = (user) => ({
    type: 'LOGIN_SUCCESS',
    payload: user,
  });
  
  export const loginFailure = (error) => ({
    type: 'LOGIN_FAILURE',
    payload: error,
  });
  
  export const registerSuccess = (user) => ({
    type: 'REGISTER_SUCCESS',
    payload: user,
  });
  
  export const registerFailure = (error) => ({
    type: 'REGISTER_FAILURE',
    payload: error,
  });
  
  export const logout = () => ({
    type: 'LOGOUT',
  });
  
  export const clearError = () => ({
    type: 'CLEAR_ERROR',
  });
  