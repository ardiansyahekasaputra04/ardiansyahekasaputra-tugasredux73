import { combineReducers } from 'redux'
// import dataReducer from './RDC/dataReducer';
import homeReducer from './RDC/homeRdc';
import cartReducer from './RDC/cartRdc';
import accountReducer from './RDC/AccountRdc';

const rootReducer = combineReducers({
    account: accountReducer,
    toko: homeReducer,
    barang: cartReducer
})

export default rootReducer;