import React, { useState } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    StyleSheet,
    ScrollView,
    KeyboardAvoidingView,
    Dimensions,
    Image
} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import Icons from 'react-native-vector-icons/Entypo'
import { useDispatch, useSelector } from "react-redux";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment'
import ImageCropPicker from 'react-native-image-crop-picker'

const DataToko = ({
    navigation,
    route
}) => {
    const { toko } = useSelector((state) => state.toko);
    const dispatch = useDispatch()
    const [idStore, setIdStore] = useState(moment(new Date).format('YYYYMMDDHHmmssSS'));
    const [address, setAddress] = useState('');
    const [openTime, setOpenTime] = useState('');
    const [closedTime, setClosedTime] = useState('');
    const [descriptions, setDescriptions] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const [minimumPrice, setMinimumPrice] = useState(0);
    const [maximumPrice, setMaximumPrice] = useState(0);
    const [rating, setRating] = useState(0);
    const [storeImage, setStoreImage] = useState('');
    const [storeName, setStoreName] = useState('');
    const [openTimePickerVisible, setOpenTimePickerVisible] = useState('')
    const [closedTimePickerVisible, setClosedTimePickerVisible] = useState('')

    //timepicker
    const showOpenTimePicker = () => {
        setOpenTimePickerVisible(true);
    };

    const hideOpenTimePicker = () => {
        setOpenTimePickerVisible(false);
    };

    const handleOpenTimeConfirm = (date) => {
        const formattedTime = moment(date).format('HH:mm');
        setOpenTime(formattedTime);
        hideOpenTimePicker();
    };

    const showClosedTimePicker = () => {
        setClosedTimePickerVisible(true);
    };

    const hideClosedTimePicker = () => {
        setClosedTimePickerVisible(false);
    };

    const handleClosedTimeConfirm = (date) => {
        const formattedTime = moment(date).format('HH:mm');
        setClosedTime(formattedTime);
        hideClosedTimePicker();
    };

    //ImagePicker
    const selectStoreImage = () => {
        ImageCropPicker.openPicker({
            cropping: true,
            includeBase64: true,
            compressImageQuality: 0.8,
            freeStyleCropEnabled: true,
            cropperStatusBarColor: 'white',
            cropperToolbarColor: 'white',
            cropperToolbarTitle: 'Crop Image',
            cropperToolbarWidgetColor: '#2f80ed',
        })
            .then((image) => {
                setStoreImage(image.path);
            })
            .catch((error) => {
                console.log(error);
            });
    };



    //utama
    const addData = async () => {
        if (!idStore || !address || !openTime || !closedTime || !descriptions || !isOpen || !minimumPrice || !maximumPrice || !storeImage || !storeName) {
            alert('Semua kolom harus diisi!');
            return;
        }
        if (rating > 6) {
            alert('Pilih 1-5')
            return
        }
        if (minimumPrice && maximumPrice < 1000) {
            alert('Harga tidak boleh dibawah Rp 1000,-')
            return
        }
        if (openTime && closedTime == 0) {
            alert('Silahkan atur jam, kalo nggak pake tombol lah :v')
            return
        }
        var dataToko = [...toko]
        const data = {
            id: idStore,
            address: address,
            openTime: openTime,
            closedTime: closedTime,
            descriptions: descriptions,
            isOpen: isOpen,
            minimumPrice: minimumPrice,
            maximumPrice: maximumPrice,
            rating: rating,
            storeImage: storeImage,
            storeName: storeName
        }
        dataToko.push(data)
        dispatch({ type: 'ADD_TOKO', data: dataToko })
        navigation.goBack()
        console.log("ADD", data)
    }
    const checkData = () => {
        if (route.params) {
            const data = route.params.item;
            setIdStore(data.id)
            setAddress(data.address);
            setStoreImage(data.storeImage);
            setOpenTime(data.openTime);
            setClosedTime(data.closedTime);
            setDescriptions(data.descriptions);
            setIsOpen(data.isOpen);
            setMinimumPrice(data.minimumPrice);
            setMaximumPrice(data.maximumPrice);
            setRating(data.rating);
            setStoreImage(data.storeImage);
            setStoreName(data.storeName);
        }
        console.log('CHECK')
    };

    React.useEffect(() => {
        checkData();
    }, []);
    const updateData = async () => {
        const data = {
            id: idStore,
            address: address,
            storeImage: storeImage,
            openTime: openTime,
            closedTime: closedTime,
            descriptions: descriptions,
            isOpen: isOpen,
            minimumPrice: minimumPrice,
            maximumPrice: maximumPrice,
            rating: rating,
            storeName: storeName,
        };
        dispatch({ type: 'UPDATE_TOKO', data });
        navigation.goBack();
        console.log('UPDATE', data)
    };
    const deleteData = async () => {
        dispatch({ type: 'DELETE_TOKO', id: idStore })
        navigation.goBack()
        console.log('DELETE', id)
    }



    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}
            >
                <KeyboardAvoidingView behavior='padding' enabled keyboardVerticalOffset={-500}>
                    <View style={{ backgroundColor: '#fff', padding: 20, width: '100%', height: Dimensions.get('window').height + 155 }}>
                        <Text style={styles.title}>Tambahkan Data</Text>
                        <View style={styles.input}>
                            <Text style={styles.dataInput}>Nama Toko</Text>
                            <TextInput
                                placeholder='Masukkan Nama Toko'
                                style={styles.txtInput}
                                value={storeName}
                                onChangeText={setStoreName}
                                keyboardType='default'
                            />
                            <Text style={styles.dataInput}>Alamat</Text>
                            <TextInput
                                placeholder='Masukkan Alamat'
                                style={styles.txtInput}
                                value={address}
                                onChangeText={setAddress}
                                keyboardType='default'
                            />
                            <Text style={[styles.dataInput]}>Status</Text>
                            <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                                <TouchableOpacity
                                    onPress={() => setIsOpen('Buka')}
                                    style={{
                                        height: 25,
                                        width: 25,
                                        borderWidth: 1,
                                        borderRadius: 15,
                                        backgroundColor: isOpen == 'Buka' ? 'green' : 'white',
                                    }}
                                >
                                    {isOpen == 'Buka' ? (
                                        <Icon name="check" size={20} color="white" />
                                    ) : null}
                                </TouchableOpacity>
                                <Text style={styles.dataStatus}>Buka</Text>
                                <TouchableOpacity
                                    onPress={() => setIsOpen('Tutup')}
                                    style={{
                                        height: 25,
                                        width: 25,
                                        borderWidth: 1,
                                        borderRadius: 15,
                                        marginLeft: 90,
                                        backgroundColor: isOpen == 'Tutup' ? 'red' : 'white',
                                    }}
                                >
                                    {isOpen == 'Tutup' ? (
                                        <Icon name="check" size={20} color="white" />
                                    ) : null}
                                </TouchableOpacity>
                                <Text style={styles.dataStatus}>Tutup</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flexDirection: 'column' }}>
                                    <Text style={styles.dataInput}>Jam Buka</Text>
                                    <View style={{ flexDirection: 'row', borderWidth: 1, borderRadius: 6, width: 145, marginRight: 10, alignItems: 'center', borderColor: '#dedede', height: 51 }}>
                                        <Text
                                            style={[styles.txtInputJam, { width: 120, borderWidth: -1 }]}
                                        >{openTime || 'Pilih Jam'} </Text>
                                        <TouchableOpacity onPress={showOpenTimePicker}>
                                            <Icon name="clockcircle" size={16} color='grey' />
                                        </TouchableOpacity>
                                    </View>
                                    <DateTimePickerModal
                                        isVisible={openTimePickerVisible}
                                        mode="time"
                                        onConfirm={handleOpenTimeConfirm}
                                        onCancel={hideOpenTimePicker}
                                    />
                                </View>
                                <View stye={{ flexDirection: 'column' }}>
                                    <Text style={styles.dataInput}>Jam Tutup</Text>
                                    <View style={{ flexDirection: 'row', borderWidth: 1, borderRadius: 6, width: 145, marginRight: 10, alignItems: 'center', borderColor: '#dedede', height: 51 }}>
                                        <Text
                                            style={[styles.txtInputJam, { width: 120, borderWidth: -1 }]}
                                        >{closedTime || 'Pilih Jam'} </Text>
                                        <TouchableOpacity onPress={showClosedTimePicker}>
                                            <Icon name="clockcircle" size={16} color='grey' />
                                        </TouchableOpacity>
                                    </View>
                                    <DateTimePickerModal
                                        isVisible={closedTimePickerVisible}
                                        mode="time"
                                        onConfirm={handleClosedTimeConfirm}
                                        onCancel={hideClosedTimePicker}
                                    />
                                </View>
                            </View>
                            <Text style={styles.dataInput}>Jumlah Rating</Text>
                            <TextInput
                                placeholder='Masukkan Rating'
                                style={styles.txtInput}
                                value={rating}
                                onChangeText={setRating}
                                keyboardType='number-pad'
                            />
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flexDirection: 'column' }}>
                                    <Text style={styles.dataInput}>Harga Minimal</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TextInput
                                            placeholder='Tulis Harga'
                                            style={[styles.txtInputJam, { width: 145, marginRight: 10 }]}
                                            value={minimumPrice}
                                            onChangeText={setMinimumPrice}
                                            keyboardType='numeric'
                                        />
                                    </View>
                                </View>
                                <View stye={{ flexDirection: 'column' }}>
                                    <Text style={styles.dataInput}>Harga Maksimal</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TextInput
                                            placeholder='Tulis Harga'
                                            style={[styles.txtInputJam, { width: 145 }]}
                                            value={maximumPrice}
                                            onChangeText={setMaximumPrice}
                                            keyboardType='numeric'
                                        />
                                    </View>
                                </View>
                            </View>
                            <Text style={styles.dataInput}>Deskripsi</Text>
                            <TextInput
                                placeholder='Masukkan Deskripsi'
                                multiline
                                style={[styles.txtInput, { height: 100, textAlignVertical: 'top' }]}
                                value={descriptions}
                                onChangeText={setDescriptions}
                                keyboardType='default'
                            />
                            <Text style={styles.dataInput}>Gambar Toko</Text>
                            <View style={styles.storeImageContainer}>
                                <TouchableOpacity onPress={selectStoreImage} style={styles.selectImageButton}>
                                    {storeImage ? (
                                        <Image source={{ uri: storeImage }} style={styles.storeImage} />
                                    ) : (
                                        <Icons name="image" size={30} color='grey' />
                                    )}
                                </TouchableOpacity>
                                <Text style={styles.selectImageButtonText}>Pilih Gambar</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity
                                style={styles.save}
                                onPress={() => {
                                    if (route.params) {
                                        updateData()
                                    } else {
                                        addData()
                                    }
                                }}
                            >
                                <Text style={styles.dataInput}>
                                    {route.params ? 'Ubah' : 'Tambah'}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.delete} onPress={deleteData}>
                                <Icons
                                    name="trash"
                                    size={20}
                                    color='black'
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </ScrollView>
        </View >
    );
}

const styles = StyleSheet.create({
    title: {
        fontFamily: 'Monserrat',
        fontSize: 24,
        fontStyle: 'normal',
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 20
    },
    input: {
        width: '100%',
        height: '85%',
        backgroundColor: '#FFf',
        padding: 10,
        marginBottom: 30
    },
    dataInput: {
        fontFamily: 'Monserrat',
        fontSize: 14,
        fontStyle: 'normal',
        fontWeight: '700',
        color: 'black',
        marginBottom: 5
    },
    txtInput: {
        width: '100%',
        borderRadius: 6,
        borderColor: '#dedede',
        borderWidth: 1,
        paddingHorizontal: 10,
        marginBottom: 5
    },
    txtInputJam: {
        width: '100%',
        borderRadius: 6,
        borderColor: '#dedede',
        paddingHorizontal: 10,
        borderWidth: 1
    },
    dataStatus: {
        fontFamily: 'Monserrat',
        fontSize: 18,
        fontStyle: 'normal',
        fontWeight: '500',
        color: 'black',
        marginLeft: 5
    },
    storeImageContainer: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    storeImage: {
        width: 50,
        height: 100,
    },
    selectImageButton: {
        backgroundColor: 'gray',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
        marginRight: 10
    },
    selectImageButtonText: {
        color: 'white',
        fontWeight: 'bold',
    },
    save: {
        width: '85%',
        height: 40,
        backgroundColor: 'grey',
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    delete: {
        width: 40,
        height: 40,
        backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default DataToko;
